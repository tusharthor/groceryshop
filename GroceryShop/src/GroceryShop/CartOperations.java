package GroceryShop;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

//Business Logic Class
public class CartOperations {
	
	HashMap<String, Double> groceryItems = new HashMap<>();
	LinkedHashMap<String, Double> cartItems = new LinkedHashMap<>();
	double totalAmount = 0;
	//non-static block
	{
		groceryItems.put("sugar", 45.0);
		groceryItems.put("tea", 70.0);
		groceryItems.put("milk", 30.0);
		groceryItems.put("tomato", 25.0);
		groceryItems.put("potato", 20.0);
	}
	
	void viewAllItems() {
		//display all grocery items
		Set<Map.Entry<String, Double>> items = groceryItems.entrySet();
		System.out.println("Item-Name\t\tPrice");
		System.out.println("==========================");
		for(Map.Entry<String, Double> entry : items) {
			System.out.println(entry.getKey()+"\t\t"+entry.getValue());
		}
		System.out.println("============================");
	}
	
	void addItemToCart(String name, int qty) {
		//add items to cart
		boolean status = groceryItems.containsKey(name); //check whether key is present and return boolean
		if(status) {
			cartItems.put(name, groceryItems.get(name)*qty); //get(name) -> return value of key (name)
			System.out.println("items added successfully");
			System.out.println("============================");
		}
		else {
			System.out.println("Item is not available");
			System.out.println("============================");
		}
	}
	
	void removeCartItems(String rname) {
		//remove cart items
		boolean status = cartItems.containsKey(rname);  //check item is present in cart or not
		if(status) {
			cartItems.remove(rname);
			System.out.println("item removed successfully");
			System.out.println("============================");
		}
		else {
			System.out.println("first add to cart");
			System.out.println("============================");
		}
		
	}
	
	void viewCartItems() {
		//view cart
		Set<Map.Entry<String, Double>> items = cartItems.entrySet();
		System.out.println("Item-Name\t\tTotal Price");
		System.out.println("==========================");
		for(Map.Entry<String, Double> entry : items) {
			System.out.println(entry.getKey()+"\t\t"+entry.getValue());
		}
		System.out.println("============================");
	}
	
	void checkout() {
		//calculate total bill amount with description
		System.out.println("Item-Name\t\tUnit Price\t\tTotal");
		Set<Map.Entry<String, Double>> items = cartItems.entrySet(); //return all pairs
		for(Map.Entry<String, Double> e : items) {
			System.out.println(e.getKey()+"\t\t"+groceryItems.get(e.getKey())+"\t\t"+e.getValue());
			totalAmount += e.getValue();
		}
		System.out.println("================");
		System.out.println("Total amount"+totalAmount);
	}
	
	void billPayment(double amount) {
		//settle bill payment
		if(amount==totalAmount) {
			totalAmount-=amount;
			System.out.println("bill paymet successful");
			cartItems.clear();  //clear the cart after successful payment (method will remove all items)
		}
		else {
			System.out.println("enter correct amount");
		}
	}
}
