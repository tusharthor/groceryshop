package GroceryShop;

import java.util.Scanner;

public class GroceryShopSimulator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		CartOperations c = new CartOperations();  //businessLogic class object
		boolean repeat = true;
		
		while(repeat) {
			
			System.out.println("welcome to grocery shop");
			System.out.println("\n1. view grocery items");
			System.out.println("2. add grocery item to cart");
			System.out.println("3. remove items from cart");
			System.out.println("4. view all cart items");
			System.out.println("5.checkout");
			System.out.println("6.pay bill");
			System.out.println("7.exit");
			
			int choice = sc.nextInt();
			
			switch(choice) {
			
			case 1:
				//System.out.println("view grocery items");
				c.viewAllItems();
				break;
			
			case 2:
				System.out.println("enter item name");
				String name = sc.next();
				System.out.println("enter quantity");
				int qty = sc.nextInt();
				//call method
				c.addItemToCart(name, qty);
				//System.out.println("add grocery items to cart");
				break;
				
			case 3:
				System.out.println("enter item name to remove ");
				String removeName = sc.next();
				c.removeCartItems(removeName);
				//System.out.println("remove items from cart");
				break;
			case 4:
				c.viewCartItems();
				//System.out.println("view all cart items");
				break;
			case 5:
				c.checkout();
				//System.out.println("checkout");
				break;
			case 6:
				System.out.println("enter amount to be paid");
				double amount = sc.nextDouble();
				c.billPayment(amount);
				//System.out.println("pay bill");
				break;
			case 7:
				if(c.totalAmount==0.0) {
					repeat = false;  //means payment successful
					break;
				}
				else {
					System.out.println("first make bill payment");
				}
				//System.out.println("exit");
				break;
				default:
					System.out.println("invalid choice");
					break;
			}
		}
	}

}
